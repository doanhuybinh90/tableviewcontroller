//
//  TableViewController.swift
//  TableViewController
//
//  Created by Doan Huy Binh on 9/13/16.
//  Copyright © 2016 Doan Huy Binh. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    var AcronymData = ["BRA","CAN","CHI","GER","IND","JAP","SOU","SPA","UK","USA"]
    
    var Countries = ["Brazil","Canada","China","Germany","Indonesia","Japan","Southafrica","Spain","United Kingdom","United States of Ameria" ]
    
    var ImageData = ["brazil.png","canada.png","china.png","germany.png","indonesia.png","japan.png","southafrica.png","spain.png","uk.png",
                     "usa.png"]
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return AcronymData.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MyCustomTableViewCell
        
        //         cell.textLabel!.text = AcronymData[indexPath.row]
        //        cell.detailTextLabel!.text = Countries[indexPath.row]
        cell.abbrLabel.text = AcronymData[(indexPath as NSIndexPath).row]
        cell.nameLabel.text = Countries[indexPath.row]
        cell.flatImage.image = UIImage(named: ImageData[indexPath.row])
        return cell
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        
        if editingStyle == .delete {
            AcronymData.remove(at: (indexPath as NSIndexPath).row)
            Countries.remove(at: (indexPath as NSIndexPath).row)
            ImageData.remove(at: (indexPath as NSIndexPath).row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            
        }
    }
    
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        
        let Alert = UIAlertController(title: "Accessory Tapped", message: "You have Accessory Tapped for \(AcronymData[(indexPath as NSIndexPath).row])", preferredStyle: .alert)
        
        Alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        self.present(Alert, animated: true, completion: nil)
        
        
    }
    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to toIndexPath: IndexPath) {
        
        let tempAcronymData1 = AcronymData[(fromIndexPath as NSIndexPath).row]
        let tempAcronymData2 = AcronymData[(toIndexPath as NSIndexPath).row]
        
        AcronymData[(fromIndexPath as NSIndexPath).row] = tempAcronymData2
        AcronymData[(toIndexPath as NSIndexPath).row] = tempAcronymData1
        
        
        let tempCountry1 = Countries[(fromIndexPath as NSIndexPath).row]
        let tempCountry2 = Countries[(toIndexPath as NSIndexPath).row]
        
        Countries[(fromIndexPath as NSIndexPath).row] = tempCountry2
        Countries[(toIndexPath as NSIndexPath).row] = tempCountry1
        
        let tempImageData1 = ImageData[(fromIndexPath as NSIndexPath).row]
        let tempImageData2 = ImageData[(toIndexPath as NSIndexPath).row]
        
        ImageData[(fromIndexPath as NSIndexPath).row] = tempImageData2
        ImageData[(toIndexPath as NSIndexPath).row] = tempImageData1
        
        tableView.reloadData()
    }
    
    
    
    // Override to support conditional rearranging of the table view.
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showDetails"
        {
            let destinationViewController: ViewController = segue.destination as! ViewController
            
            let indexPath = tableView.indexPathForSelectedRow?.row
            
            destinationViewController.abbrText = AcronymData[indexPath!]
            
            destinationViewController.nameText = Countries[indexPath!]
            
            destinationViewController.flatImageText = ImageData[indexPath!]
        }
    }
    
}
/*
*/
