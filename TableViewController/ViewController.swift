//
//  ViewController.swift
//  TableViewController
//
//  Created by Doan Huy Binh on 9/14/16.
//  Copyright © 2016 Doan Huy Binh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var flatImage: UIImageView!
    @IBOutlet weak var abbrLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    var abbrText: String = ""
    var nameText: String = ""
    var flatImageText: String = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
            if abbrText != ""
            {
                abbrLabel.text = abbrText
                nameLabel.text = nameText
                flatImage.image = UIImage(named: flatImageText)
        }
          }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
